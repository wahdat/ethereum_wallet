import { useEffect, useState } from "react";

const APIKEY = "zD5HhpV55zEixR25ncBJx1vPMYHBt0WD";
console.log("appp "+APIKEY);

const useFetch = ({ keyword }) => {
  const [gifUrl,setGifUrl] = useState("");
  

  const fetchGifs = async () => {
    try {
      const api_url = `https://api.giphy.com/v1/gifs/search?api_key=${APIKEY}&q=${keyword.split(" ").join("")}&limit=1`;
      console.log(api_url);
      const response = await fetch(api_url);
      const { data } = await response.json();

      console.log("resp",response);
      setGifUrl(data[0]?.images?.downsized_medium.url);
      console.log(data[0]?.images?.downsized_medium.url)
    } catch (error) {
        console.log("err",error);
      setGifUrl("https://metro.co.uk/wp-content/uploads/2015/05/pokemon_crying.gif?quality=90&strip=all&zoom=1&resize=500%2C284");
    }
  };

  useEffect(() => {
    if (keyword) fetchGifs();
  }, [keyword]);

  return gifUrl;
};

export default useFetch;