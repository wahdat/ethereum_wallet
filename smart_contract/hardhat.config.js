// https://eth-ropsten.alchemyapi.io/v2/pMwUwwSqXtjTZaeA4hR7sKt2DpxTT_V0

require('@nomiclabs/hardhat-waffle');
module.exports = {
  solidity :'0.8.0',
  networks: {
    ropsten:{
      url: 'https://eth-ropsten.alchemyapi.io/v2/pMwUwwSqXtjTZaeA4hR7sKt2DpxTT_V0',
      accounts:['7f6267bc3f3c4374e6612d89eac385d96197c30207a062831b58c97e89252173']
    }
  }

}